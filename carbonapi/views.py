import django_filters

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from .serializers import UserSerializer, UsageTypeSerializer, UsageSerializer
from .models import User, UsageType, Usage


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class TimeRangeFilter(django_filters.FilterSet):
    class Meta:
        model = Usage
        fields = {
            'usage_at': ['gt', 'lt']
        }


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend,
                       filters.SearchFilter,
                       filters.OrderingFilter]

    filterset_fields = ['id', 'name']
    search_fields = ['id', 'name']
    ordering_fields = ['id', 'name']


class UsageTypeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = UsageType.objects.all()
    serializer_class = UsageTypeSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend,
                       filters.SearchFilter,
                       filters.OrderingFilter]

    filterset_fields = ['id', 'name', 'unit', 'factor']
    search_fields = ['id', 'name', 'unit', 'factor']
    ordering_fields = ['id', 'name', 'unit', 'factor']


class UsageViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Usage.objects.all()
    serializer_class = UsageSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend,
                       filters.SearchFilter,
                       filters.OrderingFilter]
    filterset_class = TimeRangeFilter

    filterset_fields = ['id', 'amount', 'usage_type_id', 'user_id']
    search_fields = ['id', 'usage_at', 'amount', 'usage_type_id', 'user_id']
    ordering_fields = ['id', 'usage_at', 'amount', 'usage_type_id', 'user_id']
