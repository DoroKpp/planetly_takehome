from django.contrib import admin

# Register your models here.
from carbonapi.models import User, Usage, UsageType

admin.site.register(User)
admin.site.register(Usage)
admin.site.register(UsageType)