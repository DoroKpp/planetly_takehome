from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework.schemas import get_schema_view

from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'usagetypes', views.UsageTypeViewSet)
router.register(r'usages', views.UsageViewSet)

# wire up API using automatic URL routing.
# include login URLs for the browsable API.
urlpatterns = [
    path('carbon-api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url':'api_schema'}
    ), name='swagger-ui'),
    path('api-schema/', get_schema_view(title='Title', description='descr'), name='api_schema'),
]
