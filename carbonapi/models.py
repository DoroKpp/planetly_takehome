from django.db import models


# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name


class UsageType(models.Model):
    name = models.CharField(max_length=60)
    unit = models.CharField(max_length=20)
    factor = models.FloatField()

    def __str__(self):
        return self.name


class Usage(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    usage_type = models.ForeignKey(UsageType, on_delete=models.CASCADE)
    usage_at = models.DateTimeField()
    amount = models.FloatField()

    def __str__(self):
        return f'{self.user}, {self.usage_type}, {self.amount} {self.usage_type.unit}'
