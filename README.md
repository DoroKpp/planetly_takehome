## Welcome to a small-scale carbon api!

### Starting the application
To start the application, run `docker-compose up` 

Then create a superuser to use for testing with
`docker exec -it planetly_web_1 python3 manage.py createsuperuser`

### Using the application
To get an overview over all the endpoints and methods got to
> http://0.0.0.0:8000/swagger-ui/

The API contains three endpoints for which the methods GET, 
POST, PUT, PATCH and DELETE can be called. 

All these methods can only be called by a logged-in user. 
The login is possible at
> http://0.0.0.0:8000/api-auth/login/

All GET endpoints use pagination with a maximum size of 10 items per page.
They can also be filtered by all properties, e.g.
>http://0.0.0.0:8000/carbon-api/users/?name=Doro

as well as sorted by all properties, e.g.
>http://0.0.0.0:8000/carbon-api/usagetypes/?ordering=name

The usages can additionally be filtered by at time range concerning the field "usage_at", e.g.
>http://0.0.0.0:8000/carbon-api/usages/?usage_at__gt=2020-05-11T22:00:00&usage_at__lt=2020-05-20T22:00:00

### Completing this task
Completing this task, I had to face the challenge of using a framework I am not 
very familiar with. I have used Django before, but not in combination with Docker 
and apps like Django REST Framework, which seems to be quite useful, but also offers
a wide range of possibilities that are difficult to take in, in only a few hours. 

Thus, it took me a lot longer than the expected 2 to 4 hours - more like 8 hours. 
And I am still not completely satisfied with my work because the implemented 
authentication is not token-based, and I did not have time for testing in the end.

### Thank you...
...for taking the time to look at my work. I hope you find everything you need.

Cheers!
